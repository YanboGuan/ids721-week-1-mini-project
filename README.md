[Link to live static site](https://ids721-week-1-mini-project-yanboguan-a6cbd3f04e26e6e3cf2a1001f0.gitlab.io)
# ids721-mini-project1
I have created a static site with Zola, a Rust static site generator, that will hold all of the portfolio work in this ids721. Store source code in a GitLab repo in our Duke GitLab organization.
## Overview of the website
Please click on the Projects button on the top-right corner of the page to navigate the project contents. Home button is located on the top left corner

### Home Page

- Introduction of the course, and brief overview
![Home Page](home_page.png)

### Project Page

-  Include a list of projects
![Project list](project_page.png)

### Project Contents

-  Details and GitLab repo for the project
![Project Content](project_content.png)

### About

-  Introduction of myself
![About](about.png)