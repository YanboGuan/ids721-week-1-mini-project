+++
title = "About myself"
template = "about.html"
+++

Hello, my name is Guan Yanbo and I am currently a student at Duke University, majoring in Electrical and Computer Engineering with a focus on the software track. 

Previously, I have gained valuable experience working in the telecommunications industry, which provided me with a solid foundation in technology and its practical applications. I am now transitioning my career towards software development, where I am excited about the potential to innovate and create impactful solutions.

I am proficient in programming languages such as C/C++ and Python, which have been instrumental in my academic and professional projects. Furthermore, I am skilled in using Matlab for modeling software, which complements my programming abilities and allows me to tackle complex engineering problems. Meanwhile, I am a amateur photographer, here is a photo I take in Zermatt.

![Alt Text](photo.jpg)


