+++
title = "Introduction of the Course"
template = "index.html"
+++

## Overview

Welcome to the homepage of my website. Here you can find information about me and my work. Please click on the Projects button on the top-right corner of the page to navigate the project contents. Home button is located on the top left corner
## IDS-721 Cloud Computing for Data Analysis.
- Term: Spring 2024
- Date:  01/10/2024 - 04/17/2024

## Course Description:

- This course is designed to give you a comprehensive view of cloud computing including Big Data, Machine Learning and Large Language Models (LLMS). A variety of learning resources will be used including interactive labs on AWS. This is a project-based course with extensive hands-on assignments and the language will be exclusively used.

## Learning Objectives:
- Summarize the fundamentals of cloud computing
- Achieve AWS Solutions Architect Certification
- Master the Rust language for general programming
- Effectively utilize AI Pair Programming
- Evaluate the economics of cloud computing
- Accurately evaluate distributed computing challenges and opportunities and apply this knowledge to real-world projects.
- Develop non-linear life-long learning skills
- Build, share and present compelling portfolios using: GitLab, Hugging Face, YouTube, Linkedin and a personal portfolio website.
- Develop Metacognition skills (By teaching we learn)

## Core Tech Stack
- Rust
- AWS (AWS Learner Labs, Optional Free Tier Account)
- GitLab
- Slack
- AWS Lightsail for Research (GPU)
- CodeWhisperer

## Projects
- Project 1: Continuous Delivery of Personal Website
- Project 2: Continuous Delivery of Rust Microservice
- Project 3: Rust Vector Database or Polar DataFrame API
- Project 4: Rust AWS Lambda and Step Functions
- Final Project: LLMOps - Model Serving with Rust

