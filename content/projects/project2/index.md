+++
title = "project2"
date = 2024-02-09
[extra]
summary = "This is a brief description of my first project."
+++

Here is the detailed description of my 2nd project...

- Link to mini-project 2: [GitLab repo](https://gitlab.com/YanboGuan/ids721-week-2-project.git )
# Details
This AWS Lambda function that interacts with Amazon S3 to perform sum operation of a list on a CSV file. 

## Requirements
- Rust Lambda Function using Cargo LambdaLinks to an external site.
- Process and transform sample data

## Deliverables
- Lambda functionality: 30%
- API Gateway integration: 30%
- Data processing: 30%
- Documentation: 10%