+++
title = "mini-project1"
date = 2024-01-29
description = "This is a start of my personal website"
[extra]
summary = "This is a brief description of my first project."
+++
- Link to mini-project 1: [GitLab repo](https://gitlab.com/YanboGuan/ids721-week-1-mini-project.git)
# Details
I have created a static site with Zola, a Rust static site generator, that will hold all of the portfolio work in this ids721.  Store source code in a GitLab repo in our Duke GitLab organization.

## Requirements
- Static site generated with Zola
- Home page and portfolio project page templates
- Styled with CSS
- GitLab repo with source code

## Deliverables
- Link to live static site or screenshot showing it running
- Link to GitLab repo